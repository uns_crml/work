
import numpy as np
import tensorflow as tf
from PIL import Image
import glob

im_list = glob.glob('train/*.tif')
num_list = ([np.array(Image.open(im), dtype=np.float32).flatten() for im in im_list])
train_x = np.array(num_list[::2], dtype=np.float32)
train_y = np.array(num_list[1::2], dtype=np.float32)

W1 = tf.Variable(tf.truncated_normal([420*580, 42], stddev=0.01))
b1 = tf.Variable(tf.truncated_normal([42], stddev=0.01))
W2 = tf.Variable(tf.truncated_normal([42, 420*580], stddev=0.01))
b2 = tf.Variable(tf.truncated_normal([420*580], stddev=0.01))

y1 = tf.nn.relu(tf.matmul(train_x, W1) + b1)
y2 = tf.matmul(y1, W2) + b2
loss = tf.reduce_mean(tf.square(train_y - y2))
train = tf.train.RMSPropOptimizer(0.01).minimize(loss, var_list=[W1, b1, W2, b2])



init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init)
    for epoch in range(5):
        sess.run(train)
        acc = loss.eval()
        print('acc %g' % acc)

